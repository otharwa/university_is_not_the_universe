/*****************************************************************
*******************************************************************
  PARCIAL DE JAVASCRIPT - DW2 PROGRAMACION I - TP #3 DOM AVANZADO
  AUTOR: Arca Sebastian
*******************************************************************
*******************************************************************/

//DE ACA EN ADELANTE, TU PARCIAL....

/*------------- Parte 1 - CONCURSO ------------------------*/
validationConcurso = { 
	data:[],
	actionElement:false
}
validationConcurso.update = function(){
	if( validationConcurso.actionElement == false)
		return false;
	if( validationConcurso.data.length == 0)
		return false;
	var count = 0;
	for( var i = 0; i < validationConcurso.data.length; i++){
		if( validationConcurso.data[i] )
			count++;
		else
			count--;
	}
		if(count >=5 )
		validationConcurso.actionElement.removeAttribute('disabled');
	else 
		validationConcurso.actionElement.setAttribute('disabled', 'disabled');
	
	return true;
}
validationConcurso.create = function () {
	concursoModal = cDom([
		{objetoId:'divModal', etiqueta: 'div', id:'modal'},
		{objetoId:'form', etiqueta:'form', method:'post', action:'#'},
		{objetoId:'divCerrar', etiqueta:'div'},
		{objetoId:'aCerrar', etiqueta:'a', href:'#'},
		{objetoId:'h2', etiqueta: 'h2', texto: 'Concurso!!!'},
		{objetoId:'divNombre', etiqueta:'div'},
		{objetoId:'spanNombre', etiqueta:'span', texto: 'Tu Nombre' },
		{objetoId:'inputNombre', etiqueta:'input', type:'text', name:'nombre'},
		{objetoId:'divEmail', etiqueta:'div'},
		{objetoId:'spanEmail', etiqueta:'span', texto:'Tu Email'},
		{objetoId:'inputEmail', etiqueta:'input', type:'text', name:'nombre'},
		{objetoId:'divGenero', etiqueta:'div'},
		{objetoId:'spanGenero', etiqueta: 'span', texto:'Genero preferido (mínimo 4 generos)'},
		{objetoId:'divGeneros', etiqueta:'div', class:'generos'},
		{objetoId:'divButton', etiqueta:'div'},
		{objetoId:'button', etiqueta:'input', type:'button', value:'Participar',disabled:'disabled'}
	]);
	concursoModal.divModal.appendChild( concursoModal.form );
	// concursoModal.form.appendChild( concursoModal.divCerrar.appendChild( concursoModal.aCerrar )  );
	var cm_form  = concursoModal.form;
	var cm  = concursoModal;

	cm.divCerrar.appendChild( cm.aCerrar );
	cm_form.appendChild( cm.divCerrar );
	cm.aCerrar.onclick = function(){
		var modal = document.getElementById('modal');
		modal.parentNode.removeChild( modal );
	}

	cm_form.appendChild( cm.h2 );

	cm.divNombre.appendChild( cm.spanNombre );
	cm_form.appendChild(cm.divNombre );
	cm.divNombre.appendChild( cm.inputNombre );
	cm.inputNombre.onchange = function(){
		var valid_index = validationConcurso.data.length;
		if( (this.value != '') && (typeof this.value !== 'undefined') && (this.value.length > 2) ){
			validationConcurso.data[valid_index] = [this.value, this.checked];
		}else{
			if( validationConcurso.data[valid_index] !== 'undefined')
				validationConcurso.data[valid_index] = false;
		}
		validationConcurso.update();
	}

	cm.divEmail.appendChild( cm.spanEmail );
	cm_form.appendChild(cm.divEmail );
	cm.divEmail.appendChild( cm.inputEmail );
	cm.inputEmail.onchange = function(){
		var valid_index = validationConcurso.data.length;
		if( (this.value != '') && (typeof this.value !== 'undefined') && (this.value.length > 2) ){
			validationConcurso.data[valid_index] = [this.value, this.checked];
		}else{
			if( validationConcurso.data[valid_index] !== 'undefined')
				validationConcurso.data[valid_index] = false;
		}
		validationConcurso.update();
	}

	cm.divGenero.appendChild( cm.spanGenero );
	cm_form.appendChild(cm.divGenero );
	cm.divGenero.appendChild(cm.divGeneros);

	for( var i = 0; i < generos.length; i++){
		var name = generos[i];
		name_split = name.split(' ');
		name = (typeof name_split !== 'string') ? name_split.join('_') : name_split;
		name = name.toLowerCase();
		var inputs = cDom([
			{objetoId: 'genero_'+i, etiqueta: 'input', type:'checkbox', name:'genero_'+name, value: name, texto: generos[i] }
		]);
		var index = 'genero_'+i;
		cm.divGeneros.appendChild( inputs[index] );

		var input = inputs[index].getElementsByTagName('input')[0];
		input.onchange = function(){
			var valid_index = validationConcurso.data.length;
			if( this.checked ){
				validationConcurso.data[valid_index] = [this.value, this.checked];
			}else{
				if( validationConcurso.data[valid_index] !== 'undefined')
					validationConcurso.data[valid_index] = false;
			}
			validationConcurso.update();
		}
		
	}
	cm_form.appendChild( cm.divButton );
	cm.divButton.appendChild( cm.button );
	cm.button.onclick = function(){
		var modal = document.getElementById('modal');
		modal.parentNode.removeChild( modal );

		var texto_respuesta = document.createTextNode('Gracias USUARIO! Ya estas participando del concurso');
		var respuesta_concurso = document.getElementById('respuesta_concurso');
		respuesta_concurso.appendChild( texto_respuesta );
	}
	validationConcurso.actionElement = cm.button;

	document.body.appendChild( concursoModal.divModal );
}//	./crearModalConcurso

imgConcurso = document.getElementsByTagName('aside')[0];
imgConcurso = imgConcurso.getElementsByTagName('img')[0];
imgConcurso.onclick = validationConcurso.create;

/*------------- ./Parte 1 - CONCURSO ------------------------*/

/*------------- Parte 2 - Próximos estrenos ------------------------*/
var proximoEstreno = {
	data:[],
	actionElement:false
};
proximoEstreno.create = function(){
	var nameIndex = this.getAttribute('alt');
	name_split = nameIndex.split(' ');
	nameIndex = (typeof name_split !== 'string') ? name_split[0] : nameIndex;
	nameIndex = nameIndex.toLowerCase();
	var info = estrenos[nameIndex];

	var estrenosModal = cDom([
		{objetoId:'divModal', etiqueta: 'div', id:'modal'},
		{objetoId:'divContent', etiqueta: 'div'},
		{objetoId:'divCerrar', etiqueta:'div'},
		{objetoId:'aCerrar', etiqueta:'a', href:'#'},
		{objetoId:'h2', etiqueta: 'h2', texto: info.Titulo },
		{objetoId:'divEstreno', etiqueta:'div'},
		{objetoId:'imgEstreno', etiqueta:'img', src:'fotos/'+info.Foto },
		{objetoId:'pParrafo', etiqueta:'p', texto: info.Info},
		{objetoId:'objectEstreno', etiqueta:'object', data:'URL_DEL_VIDEO', width:'350', height:'200'},
		{objetoId:'ulDetalles', etiqueta:'ul'},

		{objetoId:'liTitulo', etiqueta:'li', texto:'Titulo:'+info.Titulo},
		{objetoId:'liEstreno', etiqueta:'li', texto:'Estreno:'+info.Ficha.Fecha},
		{objetoId:'liGenero', etiqueta:'li', texto:'Genero:'+info.Ficha.Genero},
		{objetoId:'liClasificacion', etiqueta:'li', texto:'Clasificacion:'+info.Ficha.Clasificacion},
	]);
	estrenosModal.divModal.appendChild( estrenosModal.divContent );
	var em = estrenosModal;
	var em_content = estrenosModal.divContent;

	em_content.appendChild(em.divCerrar);
	em.divCerrar.appendChild(em.aCerrar);
	em.aCerrar.onclick = function(){
		var modal = document.getElementById('modal');
		modal.parentNode.removeChild( modal );
	}
	em_content.appendChild(em.h2);

	em_content.appendChild(em.divEstreno);
	em.divEstreno.appendChild(em.imgEstreno);
	em_content.appendChild(em.pParrafo);
	em_content.appendChild(em.objectEstreno);
	em_content.appendChild(em.ulDetalles);

	em.ulDetalles.appendChild(em.liTitulo);
	em.ulDetalles.appendChild(em.liEstreno);
	em.ulDetalles.appendChild(em.liGenero);
	em.ulDetalles.appendChild(em.liClasificacion);

	document.body.appendChild( estrenosModal.divModal );
}

var proximos_estrenos = document.getElementById('proximos_estrenos');
var imgs = proximos_estrenos.getElementsByTagName('img');
for(var i = 0; i < imgs.length; i++){
	imgs[i].onclick = proximoEstreno.create;
}
/*------------- ./Parte 2 - Próximos estrenos ------------------------*/

/*------------- Parte 3 - Rankear Avengers ------------------------*/

var ranking = {
	estrellas:[],
	points:0,
	update: function(){
		var value = parseInt(this.innerHTML);
		var spanPuntaje = document.getElementById('puntaje');
		spanPuntaje.innerHTML = value;
		ranking.points = value;

		for(var i = 0; i < ranking.estrellas.length; i++ ){
			if( ranking.points >= (i+1) )
				ranking.estrellas[i].style.backgroundImage = "url('recursos/icons/estrella_llena.png')";
			else
				ranking.estrellas[i].style.backgroundImage = "url('recursos/icons/estrella_vacia.png')";
		}
	},
	loadEvent: function(){
		var puntines = document.getElementById('puntines');
		ranking.estrellas = puntines.getElementsByTagName('li');

		for( var i = 0; i < ranking.estrellas.length; i++){
			ranking.estrellas[i].onclick = ranking.update;
		}
	}
};
ranking.loadEvent();


/*------------- ./Parte 3 - Rankear Avengers ------------------------*/


/* -----------------------------------------------------------------*/
/* ----------------------------- LIBRERIA ------------------------*/
/* -----------------------------------------------------------------*/
/*
Crear Dom con seteos
====================
//Retorno : obj.dom

*** Errores conocidos: 
	- No esta especificado el caso de <input type='radius' />
	- Dificil acceso a los value de los input
	- Floja documentacion

Los unicos parametros fuera de los estandares son, "objetoId"  "etiqueta"  "list" y "texto"

*** Ejemplo de modo de uso 1:

var elemnt = cDom(
[
	{objetoId:'tilde', etiqueta:'input', type: 'checkbox', name:'cualquiera'},
	{objetoId:'contenedorGeneral', etiqueta:'div'},
	{objetoId:'campoNombre', etiqueta:'input', name:'nombre1'},
	{objetoId:'pais1', etiqueta:'select', name:'pais1', id:'pais1', list:['Argentia','Chile','Uruguay','Otro']}
]
);

**** Ejemplo de modo de uso 2: Se pueden empujar los elementos a un objeto ya existente especificandolo en el segundo parametro

cDom(
[
	{objetoId:'tildeNueva', etiqueta:'input', type: 'checkbox', name:'cualquiera'},
	{objetoId:'contenedorGeneralNuevo', etiqueta:'div'},
],
objetoConDomsAnteriores
);
*/
function cDom(etiquetas, elementToPush_){
//Entran etiquetas salen DOMs (Elementos de html)

	var doms = {};
	var retorno = true;
	
	for(var i=0; i < etiquetas.length; i++){
		var objetoId = etiquetas[i].objetoId;
		
		doms[objetoId] = document.createElement( etiquetas[i].etiqueta );
//Todos estos if se pueden reemplazar con arguments (+ una lista de etiquetas admitidas )o con un Switch case
		for(var atributo in etiquetas[i] ){
			switch(atributo){
				case 'objetoId': break;
				case 'etiqueta': break;
				case 'onclick': break;
				//case 'name': break;
				case 'list': 
					if(etiquetas[i].etiqueta == 'select' && doms[objetoId].nodeName == 'LABEL'){
						for(var j=0; j < etiquetas[i].list.length; j++){
							option = document.createElement('option');
							option.setAttribute('value',etiquetas[i].list[j] );
							textOption = document.createTextNode( etiquetas[i].list[j] );
							
							option.appendChild(textOption);
							doms[objetoId].getElementsByTagName('select')[0].appendChild(option);
						}
					}
					break;
				case 'texto':
					if(etiquetas[i].etiqueta == 'input' || etiquetas[i].etiqueta == 'select' || etiquetas[i].etiqueta == 'textarea') {
						etiquetaLabel = document.createElement('label');
						etiquetaLabel.setAttribute('name',etiquetas[i].name);
						
						texto = document.createTextNode(etiquetas[i].texto);
						
						//Segun el orden, el texto quedara a la DERECHA o a la IZQUIERDA del elemento
						//en caso de usar doms[objetoId].childNodes[0] tambien se altera la posicion en este caso el texto esta en posicion 0
						etiquetaLabel.appendChild(texto);
						etiquetaLabel.appendChild(doms[objetoId]);
						
						doms[objetoId] = etiquetaLabel;
					}
					else {
						texto = document.createTextNode(etiquetas[i].texto);
						doms[objetoId].appendChild(texto);
					}
					break;
				default:
					if( doms[objetoId].nodeName == 'LABEL' )
						doms[objetoId].childNodes[1].setAttribute(atributo, etiquetas[i][atributo]);
					else
						doms[objetoId].setAttribute(atributo, etiquetas[i][atributo]);

					break;
			}
			
		}

		
		if(typeof elementToPush_ === 'object') {
			elementToPush_[ objetoId ] = doms[objetoId];
			//elementFromPush_.push(doms[objetoId]);
			retorno = false;
		}
	}

	if(retorno) return doms;
}