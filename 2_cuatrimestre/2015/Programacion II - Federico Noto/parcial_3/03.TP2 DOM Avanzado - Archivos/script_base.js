/*************************************************************************************
****************** EL ARRAY DE GENEROS (PARA LOS CHECKBOX CONCURSO) ******************
**************************************************************************************/

var generos = [ 'Accion', 'Animacion', 'Anime', 'Artes Marciales', 'Aventura', 'Bélico', 'Biográfico', 'Catástrofe', 'Clase-B', 'Clase-Z', 'Comedia', 'Drama', 'Épico', 'Fantasía', 'Gore', 'Musical', 'Policial', 'Sci-Fi', 'Suspenso', 'Terror', 'Western' ];



/*************************************************************************************
****************** EL ARRAY DE PROXIMOS ESTRENOS (PELICULAS ABAJO) *******************
**************************************************************************************/

var estrenos = []; //array con los 3 estrenos, cada ID del article se corresponde con el INDICE del array
estrenos['ant'] = {
	Titulo : 'El Hombre hormiga',
	Foto : 'ant_man_large.jpg',
	Trailer : 'https://www.youtube.com/v/P8ksX41WyxA',
	Info: 'Dotado con la asombrosa capacidad de reducir su tamaño pero aumentar su fuerza, el experto ladrón Scott Lang deberá abrazar su héroe interior y ayudar a su mentor, el Dr. Hank Pym, a proteger el secreto que se esconde detrás de su espectacular traje de Ant-Man de una nueva generación de imponentes amenazas. Contra obstáculos aparentemente insuperables, Pym y Lang deberán planear y ejecutar un plan que salvará el mundo.',
	Ficha: {
		Genero : 'Ciencia Ficcion',
		Fecha : '16 de julio de 2015',
		Duracion : '110 minutos',
		Clasificacion : 'Mayores 13 años'
	}
}

estrenos['jurassic'] = {
	Titulo : 'Jurassic World',
	Foto : 'jurassic_world_large.jpg',
	Trailer : 'https://www.youtube.com/v/GrUs5FR_eTA',
	Info: 'Veintidós años después de lo ocurrido en Jurassic Park, la isla Nublar ha sido transformada en un parque temático, Jurassic Wold, con versiones domesticadas de algunos de los dinosaurios más conocidos. Cuando todo parece ir a la perfección y ser el negocio del siglo, un nuevo dinosaurio de especie todavía desconocida y que es mucho más inteligente de lo que se pensaba, comienza a causar estragos entre los habitantes del Parque.',
	Ficha: {
		Genero : 'Acción',
		Fecha : '11 de junio de 2015 ',
		Duracion : '110 minutos',
		Clasificacion : 'Mayores 16 años'
	}
}

estrenos['pixels'] = {
	Titulo : 'Pixels',
	Foto : 'pixels_large.jpg',
	Trailer : 'https://www.youtube.com/v/Qh83tSbbV_I',
	Info: 'Unos alienígenas malinterpretan los videojuegos clásicos como una declaración de guerra y deciden atacar la Tierra usando esos juegos como modelos. El Presidente (Kevin James) debe llamar a su mejor amigo de la infancia Sam Brenner (Adam Sandler), quien fuera campeón de videojuegos en los años 80 para que dirija a un equipo de arcaders de la vieja escuela (Peter Dinklage y Josh Gad) que pueda derrotar a los alienígenas y salvar al planeta. A ellos se unirá la teniente Coronel Violet Van Patten (Michelle Monaghan).',
	Ficha: {
		Genero : 'Animación',
		Fecha : '23 de julio de 2015 ',
		Duracion : '90 minutos',
		Clasificacion : 'Apta todo público'
	}
}


/*************************************************************************************
******************** ARRAYS DE ESTILOS (PARA EL PANEL DE CONTROL) ********************
**************************************************************************************/

var imagenesDeFondo = ['fondos/collage.jpg', 'fondos/marvel.jpg', 'fondos/lego.jpg', 'fondos/terror.jpg', 'fondos/hobbit.jpg', 'fondos/civil_war.jpg'];

var coloresFondoEncabezado = [ '#BC1313', '#3AAA2E', '#4496C7', '#DBDBDB' ];
var coloresFuenteEncabezados = [];
	coloresFuenteEncabezados['#BC1313'] = ['#a9df8b','#ffffff', '#e0df8b', '#fffc00', '#f2d11b'];
	coloresFuenteEncabezados['#3AAA2E'] = ['#5d1f63','#44631f', '#324d51', '#000000'];
	coloresFuenteEncabezados['#4496C7'] = ['#7A4E11','#1E587A', '#9ce859'];
	coloresFuenteEncabezados['#DBDBDB'] = ['#991925','#0174aa', '#54a972', '#000000', '#b9ba42', '#88b176', '#63594e', '#888787'];

var familiasTipograficas = ['Arial', 'Verdana', 'Tahoma', 'Trebuchet Ms', 'Georgia' ];



